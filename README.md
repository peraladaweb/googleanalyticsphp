# GoogleAnalyticsPHP

Hace llamadas a la api de [google analytics](https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide)

## Enviar Tipos de hits habituales
A continuación encontrarás ejemplos de cómo enviar tipos de hits habituales a Google Analytics.

#### Seguimiento de páginas
````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos la petición
$pageView = new PageViewRequest($gateway);
$pageView->setDocumentPage('/test-home');
$pageView->setDocumentTitle('Test-Home');

// enviamos la petición
$pageView->request();

````

#### Seguimiento de eventos
````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos la petición
$event = new EventRequest($gateway);
$event->setEventCategory('test-category')
    ->setEventAction('test-Action')
    ->setEventLabel('test-lavel')
    ->setEventValue('0');
    
// enviamos la petición
$event->request();

````
### Seguimiento de comercio electrónico mejorado
Para hacer peticiones de comercio electrónico mejorado, se necesita el Hit 
de comercio electronico y una petición complementaria, que puede ser de tipo PageViewRequest
o EventRequest.

#### Medición de impresiones
````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos los productos
$products = [];
for ($i = 1; $i < 3 + 1; $i ++) {
    $product = new Product();
    $product->setId((new \DateTime())->getTimestamp());
    $product->setName('Nombre Producto' . $i);
    $product->setCategory('Categoria Producto');
    $product->setBrand('Marca Producto');
    $product->setVariant('Variante Producto');

    $products[] = $product;
}

// generamos las listas
$lists = [];
for ($i = 1; $i < 2 + 1; $i ++) {
    $list = new ProductList();
    $list->setName('Primera Lista');
    $list->setProducts($products);

    $lists[] = $list;
}

// generamos el Hit
$impression = new ImpressionHit();
$impression->setLists($lists);

// generamos la petición complementaria
$pageViewRequest = new PageViewRequest($gateway);
$pageViewRequest->setDocumentPage('/test-home');
$pageViewRequest->setDocumentTitle('Test-Home');

// generamos la petición de comercio electrónico
$ecommerceRequest = new EcommerceImprovedRequest($gateway, $pageViewRequest);
$ecommerceRequest->addHit($impression);
    
// enviamos la petición
$ecommerceRequest->request();
````

#### Medición de acciones
- Hay unas acciones limitadas definidas:
    * PRODUCT_ACTION_DETAIL = 'detail'
    * PRODUCT_ACTION_CLICK = 'click'
    * PRODUCT_ACTION_ADD = 'add'
    * PRODUCT_ACTION_REMOVE = 'remove'
    * PRODUCT_ACTION_CHECKOUT = 'checkout'
    * PRODUCT_ACTION_CHECKOUT_OPTION = 'checkout_option'
    * PRODUCT_ACTION_PURCHASE = 'purchase'
    * PRODUCT_ACTION_REFUND = 'refund'

````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos los productos
..

// generamos las listas
..

// generamos el Hit
$action = new ActionHit();
$action->setAction(ActionHit::PRODUCT_ACTION_CLICK)
    ->setList($list);

// generamos la petición complementaria
$pageViewRequest = new PageViewRequest($gateway);
$pageViewRequest->setDocumentPage('/test-home');
$pageViewRequest->setDocumentTitle('Test-Home');

// generamos la petición de comercio electrónico
$ecommerceRequest = new EcommerceImprovedRequest($gateway, $pageViewRequest);
$ecommerceRequest->addHit($action);
    
// enviamos la petición
$ecommerceRequest->request();
````

#### Combinación de impresiones y acciones
Se pueden enviar más de un hit dentro de una petición de comercio electrónico mejorado

````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos los productos
..

// generamos la lista
..

// generamos el Hit
$action = new ActionHit();
$action->setAction(ActionHit::PRODUCT_ACTION_CLICK)
    ->setList($list);

$impression = new ImpressionHit();
$impression->setLists($lists);

// generamos la petición complementaria
$pageViewRequest = new PageViewRequest($gateway);
$pageViewRequest->setDocumentPage('/test-home');
$pageViewRequest->setDocumentTitle('Test-Home');

// generamos la petición de comercio electrónico
$ecommerceRequest = new EcommerceImprovedRequest($gateway, $pageViewRequest);
$ecommerceRequest->addHit($action);
$ecommerceRequest->addHit($impression);
    
// enviamos la petición
$ecommerceRequest->request();
````

#### Medición de compras
Este hit necesita de un hit auxiliar para ser enviado

````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos los productos
..

// generamos la lista
..

// generamos el Hit auxiliar
$actionHit = new ActionHit();
$actionHit->setAction(ActionHit::PRODUCT_ACTION_CLICK)
    ->setList($list);

// generamos el hit
$transactionHit = new TransactionHit();
$transactionHit->setTransaction($transaction);
$transactionHit->setActionHit($actionHit);
			
// generamos la petición complementaria
$pageViewRequest = new PageViewRequest($gateway);
$pageViewRequest->setDocumentPage('/test-home');
$pageViewRequest->setDocumentTitle('Test-Home');

// generamos la petición de comercio electrónico
$ecommerceRequest = new EcommerceImprovedRequest($gateway, $pageViewRequest);
$ecommerceRequest->addHit($transactionHit);
    
// enviamos la petición
$ecommerceRequest->request();
````

#### Medición del proceso de pedido
Estos Hits también precisa de un hit complementario

##### Medición de los pasos de pago

````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos los productos
..

// generamos la lista
..

// generamos el Hit auxiliar
$actionHit = new ActionHit();
$actionHit->setAction(ActionHit::PRODUCT_ACTION_CHECKOUT)
    ->setList($list);

// generamos el hit
$chekoutHit = new CheckoutStepHit();
$chekoutHit->setActionHit($actionHit)
    ->setCheckoutStep(1)
    ->setCheckoutStepOption('Formulario Tarifa');
			
// generamos la petición complementaria
$pageViewRequest = new PageViewRequest($gateway);
$pageViewRequest->setDocumentPage('/test-home');
$pageViewRequest->setDocumentTitle('Test-Home');

// generamos la petición de comercio electrónico
$ecommerceRequest = new EcommerceImprovedRequest($gateway, $pageViewRequest);
$ecommerceRequest->addHit($chekoutHit);
    
// enviamos la petición
$ecommerceRequest->request();
````
##### Medición de las opciones de pago
````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos los productos
..

// generamos la lista
..

// generamos el Hit auxiliar
$actionHit = new ActionHit();
$actionHit->setAction(ActionHit::PRODUCT_ACTION_CHECKOUT_OPTION)
    ->setList($list);

// generamos el hit
$chekoutHit = new CheckoutStepHit();
$chekoutHit->setActionHit($actionHit)
    ->setCheckoutStep(1)
    ->setCheckoutStepOption('FedEx');
			
// generamos la petición complementaria
$pageViewRequest = new PageViewRequest($gateway);
$pageViewRequest->setDocumentPage('/test-home');
$pageViewRequest->setDocumentTitle('Test-Home');

// generamos la petición de comercio electrónico
$ecommerceRequest = new EcommerceImprovedRequest($gateway, $pageViewRequest);
$ecommerceRequest->addHit($chekoutHit);
    
// enviamos la petición
$ecommerceRequest->request();
````

#### Medir promociones internas
##### Impresiones de promoción
````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos las promociones
$promotions = [];
for ($i = 1; $i < $number + 1; $i++) {
    $promotion = new Promotion();
    $promotion->setId('PROMO_1234');
    $promotion->setName('PROMO NAME');
    $promotion->setCreative('PROMO CREATIVE');
    $promotion->setPosition('PROMO POSITION 1');

    $promotions[] = $promotion;
}

// generamos el hit
$promotionImpressionHit = new PromotionImpressionHit();
$promotionImpressionHit->setPromotions($promotions);
			
// generamos la petición complementaria
$pageViewRequest = new PageViewRequest($gateway);
$pageViewRequest->setDocumentPage('/test-home');
$pageViewRequest->setDocumentTitle('Test-Home');

// generamos la petición de comercio electrónico
$ecommerceRequest = new EcommerceImprovedRequest($gateway, $pageViewRequest);
$ecommerceRequest->addHit($promotionImpressionHit);
    
// enviamos la petición
$ecommerceRequest->request();
````

##### Clics de la promoción
````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos las promociones
..

// generamos el hit
$promotionClickHit = new PromotionClickHit();
$promotionClickHit->setPromotions($promotions);
			
// generamos la petición complementaria
$eventRequest = new EventRequest($gateway);
$eventRequest->setEventCategory('test-category')
    ->setEventAction('test-Action')
    ->setEventLabel('test-lavel')
    ->setEventValue('0');

// generamos la petición de comercio electrónico
$ecommerceRequest = new EcommerceImprovedRequest($gateway, $eventRequest);
$ecommerceRequest->addHit($promotionClickHit);
    
// enviamos la petición
$ecommerceRequest->request();
````

#### Seguimiento de comercio electrónico
##### Petición de transacción
````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos la transacción
$transaction = new Transaction();
$transaction->setTransactionId('123456')
    ->setAffiliation('westernWear')
    ->setRevenue(50.00)
    ->setShipping(32.00)
    ->setTax(12.00)
    ->setCurrency(Transaction::CURRENCY_CODE_EUR)
;

// generamos la petición
$transactionRequest = new TransactionRequest($gateway);
$transactionRequest->setTransaction($transaction);
    
// enviamos la petición
$transactionRequest->request();
````

##### Petición de artículo
````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos la transacción
$transaction = new Transaction();
$transaction->setTransactionId('123456')
    ->setAffiliation('westernWear')
    ->setRevenue(50.00)
    ->setShipping(32.00)
    ->setTax(12.00)
    ->setCurrency(Transaction::CURRENCY_CODE_EUR)
;

// generamos el item
$item = new Item();
$item->setName('sofa')
    ->setPrice(300)
    ->setQuantity(2)
    ->setCode('u3eqds43 ')
    ->setVariation('furniture');

// generamos la petición
$itemRequest = new ItemRequest($gateway);
$itemRequest->setTransaction($transaction)
    ->setItem($item);
    
// enviamos la petición
$itemRequest->request();
````

#### Interacciones sociales
````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos la petición
$socialRequest = new SocialRequest($gateway);
$socialRequest->setSocialAction('like')
    ->setSocialNetwork('facebook')
    ->setSocialTarget('/home');
    
// enviamos la petición
$socialRequest->request();
````

#### Seguimiento de excepciones
````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos la petición
$exceptionRequest = new ExceptionRequest($gateway);
$exceptionRequest->setExceptionDescription('IOException')
    ->setExceptionIsFatal(1);
    
// enviamos la petición
$exceptionRequest->request();
````

#### Seguimiento de tipos de usuario
````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos la petición
$timingRequest = new TimingRequest($gateway);
$timingRequest->setCategory('jsonLoader')
    ->setVariable('load')
    ->setTime(5000)
    ->setLabel('jQuery')
    ->setDnsLoadTime(100)
    ->setPageDownloadTime(20)
    ->setRedirectTime(32)
    ->setTcpConnectTime(56)
    ->setServerResponseTime(12);
    
// enviamos la petición
$timingRequest->request();
````

#### Seguimiento en la aplicación o en la pantalla
````php
// generamos el Gateway
$tid = 'UA-XXXXXX-Y';
$cid = 555;
$url = 'https://www.google-analytics.com/debug/collect';
$gateway = new Gateway($tid, $cid, $url);

// generamos la petición
$screenViewRequest = new ScreenViewRequest($gateway);
$screenViewRequest->setAppName('funTImes')
    ->setAppVersion('1.5.0')
    ->setAppId('com.peralada.analytics')
    ->setAppInstallerId('com.peralada.analytics.app.installer')
    ->setScreenName('Home');
    
// enviamos la petición
$screenViewRequest->request();
````