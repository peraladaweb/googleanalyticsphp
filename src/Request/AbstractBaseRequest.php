<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 09/08/2018
 * Time: 17:24
 */

namespace Peralada\Google\GoogleAnalytics\Request;

use Peralada\Google\GoogleAnalytics\Util\Gateway;

abstract class AbstractBaseRequest
{
	const VALID_RESPONSE_VALID = 'valid';
	const VALID_RESPONSE_DESCRIPTION = 'description';
	const VALID_RESPONSE_CODE = 'code';
	protected $response;
	protected $request;
	/**
	 * @var Gateway
	 */
	protected $gateway;
	protected $t;

	public function __construct($gateway)
	{
		$this->gateway = $gateway;
	}

	/**
	 * @return mixed
	 * @throws \Exception
	 */
	public function request()
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->gateway->getUrl());
//		curl_setopt($ch, CURLOPT_URL, 'https://www.google-analytics.com/debug/collect');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->createPostFields());
		$stringResponse = curl_exec($ch);
		$this->response = json_decode($stringResponse);

		$validResponse = $this->isValidResponse();

		if (!$validResponse[self::VALID_RESPONSE_VALID]) {
			throw new \Exception($validResponse[self::VALID_RESPONSE_DESCRIPTION]);
		}

		return $this->response;
	}

	protected function isValidResponse()
	{

		foreach ($this->response->hitParsingResult as $hitParsingResult) {
			if (!$hitParsingResult->valid) {
				foreach ($hitParsingResult->parserMessage as $parseMessage) {
					return [
						self::VALID_RESPONSE_VALID => false,
						self::VALID_RESPONSE_DESCRIPTION => $parseMessage->description,
						self::VALID_RESPONSE_CODE => $parseMessage->messageCode
					];
				}
			}
		}

		return [
			self::VALID_RESPONSE_VALID => true,
			self::VALID_RESPONSE_DESCRIPTION => '',
			self::VALID_RESPONSE_CODE => ''
		];
	}

	protected function createPostFields()
	{
		$requestArray = array_merge($this->createGatewayRequest(), $this->createRequest());

		$postFields = '';
		$position = 0;
		foreach ($requestArray as $param => $value) {
			if ($position > 0) {
				$postFields .= '&';
			}
			$postFields .= $param . '=' . $value;
			$position++;
		}

		return $postFields;
	}

	protected function createGatewayRequest()
	{
		return [
			'v' => $this->gateway->getV(),
			'tid' => $this->gateway->getTid(),
			'cid' => $this->gateway->getCid(),
			't' => $this->t
		];
	}

	abstract protected function createRequest();

	/**
	 * @return mixed
	 */
	public function getResponse()
	{
		return $this->response;
	}

	/**
	 * @param $response
	 * @return $this
	 */
	public function setResponse($response)
	{
		$this->response = $response;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getGateway()
	{
		return $this->gateway;
	}

	/**
	 * @param $gateway
	 * @return $this
	 */
	public function setGateway($gateway)
	{
		$this->gateway = $gateway;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getT()
	{
		return $this->t;
	}

	/**
	 * @param $t
	 * @return $this
	 */
	public function setT($t)
	{
		$this->t = $t;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getRequest()
	{
		return $this->request;
	}

	/**
	 * @param $request
	 * @return $this
	 */
	public function setRequest($request)
	{
		$this->request = $request;

		return $this;
	}


}