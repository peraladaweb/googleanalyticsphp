<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 03/10/2018
 * Time: 11:01
 */

namespace Peralada\Google\GoogleAnalytics\Request\Ecommerce;


use Peralada\Google\GoogleAnalytics\Request\AbstractBaseRequest;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit\EcommerceHitInterface;

class EcommerceImprovedRequest extends AbstractBaseRequest
{

	protected $hits;
	protected $principalRequest;

	public function __construct($gateway, AbstractBaseRequest $principalRequest)
	{
		parent::__construct($gateway);
		$this->principalRequest = $principalRequest;
		$this->hits = [];
	}

	public function addHit(EcommerceHitInterface $hit)
	{
		$this->hits[] = $hit;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getHits()
	{
		return $this->hits;
	}

	/**
	 * @return AbstractBaseRequest
	 */
	public function getPrincipalRequest()
	{
		return $this->principalRequest;
	}

	/**
	 * @param AbstractBaseRequest $principalRequest
	 *
	 * @return $this
	 */
	public function setPrincipalRequest(AbstractBaseRequest $principalRequest)
	{
		$this->principalRequest = $principalRequest;

		return $this;
	}

	/**
	 * @param $hits
	 * @return $this
	 */
	public function setHits($hits)
	{
		$this->hits = $hits;

		return $this;
	}

	protected function createRequest()
	{
		$request = $this->principalRequest->createRequest();

		if (!empty($this->hits)) {
			$hitsRequest = [];

			foreach ($this->hits as $hit) {
				$hitsRequest =array_merge($hitsRequest, $hit->getRequest());
			}
			$request = array_merge($request, $hitsRequest);
		}

		return $request;
	}
}