<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 19/10/2018
 * Time: 12:48
 */

namespace Peralada\Google\GoogleAnalytics\Request\Ecommerce;


use Peralada\Google\GoogleAnalytics\Request\AbstractBaseRequest;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Item;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Transaction;

class ItemRequest extends AbstractBaseRequest
{
	const HIT_TYPE_ITEM = 'item';

	/**
	 * @var Item
	 */
	protected $item;
	/**
	 * @var Transaction
	 */
	protected $transaction;

	public function __construct($gateway)
	{
		$this->setT(self::HIT_TYPE_ITEM);
		parent::__construct($gateway);
	}

	protected function createRequest()
	{
		$request = [
			'ti' => $this->transaction->getTransactionId(),
			'in' => $this->item->getName(),
			'ip' => $this->item->getPrice(),
			'iq' => $this->item->getQuantity(),
			'ic' => $this->item->getCode(),
			'iv' => $this->item->getVariation(),
			'cu' => $this->transaction->getCurrency()
		];

		return $request;
	}

	/**
	 * @return mixed
	 */
	public function getItem()
	{
		return $this->item;
	}

	/**
	 * @param $item
	 * @return $this
	 */
	public function setItem($item)
	{
		$this->item = $item;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTransaction()
	{
		return $this->transaction;
	}

	/**
	 * @param $transaction
	 * @return $this
	 */
	public function setTransaction($transaction)
	{
		$this->transaction = $transaction;

		return $this;
	}
}