<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 19/10/2018
 * Time: 11:06
 */

namespace Peralada\Google\GoogleAnalytics\Request\Ecommerce;


use Peralada\Google\GoogleAnalytics\Request\AbstractBaseRequest;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Transaction;

class TransactionRequest extends AbstractBaseRequest
{
	const HIT_TYPE_TRANSACTION = 'transaction';

	/**
	 * @var Transaction
	 */
	protected $transaction;

	public function __construct($gateway)
	{
		$this->setT(self::HIT_TYPE_TRANSACTION);
		parent::__construct($gateway);
	}

	protected function createRequest()
	{
		$request = [
			'ti' => $this->transaction->getTransactionId(),
			'ta' => $this->transaction->getAffiliation(),
			'tr' => $this->transaction->getRevenue(),
			'ts' => $this->transaction->getShipping(),
			'tt' => $this->transaction->getTax(),
			'cu' => $this->transaction->getCurrency()
		];

		return $request;
	}

	/**
	 * @return mixed
	 */
	public function getTransaction()
	{
		return $this->transaction;
	}

	/**
	 * @param $transaction
	 * @return $this
	 */
	public function setTransaction($transaction)
	{
		$this->transaction = $transaction;

		return $this;
	}
}