<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 23/08/2018
 * Time: 16:38
 */

namespace Peralada\Google\GoogleAnalytics\Request;


class EventRequest extends AbstractBaseRequest
{
	const HIT_TYPE_EVENT = 'event';

	protected $eventCategory;
	protected $eventAction;
	protected $eventLabel;
	protected $eventValue;

	public function __construct($gateway)
	{
		$this->setT(self::HIT_TYPE_EVENT);
		parent::__construct($gateway);
	}

	protected function createRequest()
	{
		$request = [
			'ec' => $this->eventCategory,
			'ea' => $this->eventAction
		];

		if (!empty($this->eventLabel)) {
			$request['el'] = $this->eventLabel;
		}

		if (!empty($this->eventValue)) {
			$request['ev'] = $this->eventValue;
		}

		return $request;
	}

	/**
	 * @return mixed
	 */
	public function getEventCategory()
	{
		return $this->eventCategory;
	}

	/**
	 * @param $eventCategory
	 * @return $this
	 */
	public function setEventCategory($eventCategory)
	{
		$this->eventCategory = $eventCategory;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEventAction()
	{
		return $this->eventAction;
	}

	/**
	 * @param $eventAction
	 * @return $this
	 */
	public function setEventAction($eventAction)
	{
		$this->eventAction = $eventAction;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEventLabel()
	{
		return $this->eventLabel;
	}

	/**
	 * @param $eventLabel
	 * @return $this
	 */
	public function setEventLabel($eventLabel)
	{
		$this->eventLabel = $eventLabel;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEventValue()
	{
		return $this->eventValue;
	}

	/**
	 * @param $eventValue
	 * @return $this
	 */
	public function setEventValue($eventValue)
	{
		$this->eventValue = $eventValue;

		return $this;
	}
}