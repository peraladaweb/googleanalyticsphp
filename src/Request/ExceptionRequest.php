<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 19/10/2018
 * Time: 13:46
 */

namespace Peralada\Google\GoogleAnalytics\Request;


class ExceptionRequest extends AbstractBaseRequest
{
	const HIT_TYPE_EXCEPTION = 'exception';

	protected $exceptionDescription;
	protected $exceptionIsFatal;

	public function __construct($gateway)
	{
		$this->setT(self::HIT_TYPE_EXCEPTION);
		parent::__construct($gateway);
	}

	protected function createRequest()
	{
		$request = [
			'exd' => $this->getExceptionDescription(),
			'exf' => $this->getExceptionIsFatal()
		];

		return $request;
	}

	/**
	 * @return mixed
	 */
	public function getExceptionDescription()
	{
		return $this->exceptionDescription;
	}

	/**
	 * @param $exceptionDescription
	 * @return $this
	 */
	public function setExceptionDescription($exceptionDescription)
	{
		$this->exceptionDescription = $exceptionDescription;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getExceptionIsFatal()
	{
		return $this->exceptionIsFatal;
	}

	/**
	 * @param $exceptionIsFatal
	 * @return $this
	 */
	public function setExceptionIsFatal($exceptionIsFatal)
	{
		$this->exceptionIsFatal = $exceptionIsFatal;

		return $this;
	}


}