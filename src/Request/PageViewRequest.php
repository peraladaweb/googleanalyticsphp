<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 10/08/2018
 * Time: 12:23
 */

namespace Peralada\Google\GoogleAnalytics\Request;


class PageViewRequest extends AbstractBaseRequest
{
	const HIT_TYPE_PAGEVIEW = 'pageview';

	protected $documentPage;
	protected $documentTitle;

	public function __construct($gateway)
	{
		parent::__construct($gateway);
		$this->setT(self::HIT_TYPE_PAGEVIEW);
	}

	protected function createRequest()
	{
		$request = [];

		if (!empty($this->documentPage)) {
			$request['dp'] = $this->documentPage;
		}

		if (!empty($this->documentTitle)) {
			$request['dt'] = $this->documentTitle;
		}

		return $request;
	}

	/**
	 * @return mixed
	 */
	public function getDocumentPage()
	{
		return $this->documentPage;
	}

	/**
	 * @param $documentPage
	 * @return $this
	 */
	public function setDocumentPage($documentPage)
	{
		$this->documentPage = $documentPage;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDocumentTitle()
	{
		return $this->documentTitle;
	}

	/**
	 * @param $documentTitle
	 * @return $this
	 */
	public function setDocumentTitle($documentTitle)
	{
		$this->documentTitle = $documentTitle;

		return $this;
	}
}