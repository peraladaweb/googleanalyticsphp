<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 22/10/2018
 * Time: 10:29
 */

namespace Peralada\Google\GoogleAnalytics\Request;


class ScreenViewRequest extends AbstractBaseRequest
{
	const HIT_TYPE_SCREENVIEW = 'screenview';

	protected $appName;
	protected $appVersion;
	protected $appId;
	protected $appInstallerId;
	protected $screenName;

	public function __construct($gateway)
	{
		$this->setT(self::HIT_TYPE_SCREENVIEW);
		parent::__construct($gateway);
	}

	protected function createRequest()
	{
		$request = [
			'an' => $this->getAppName(),
			'av' => $this->getAppVersion(),
			'aid' =>  $this->getAppId(),
			'aiid' => $this->getAppInstallerId(),
			'cd' => $this->getScreenName()
		];

		return $request;
	}

	/**
	 * @return mixed
	 */
	public function getAppName()
	{
		return $this->appName;
	}

	/**
	 * @param $appName
	 * @return $this
	 */
	public function setAppName($appName)
	{
		$this->appName = $appName;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getAppVersion()
	{
		return $this->appVersion;
	}

	/**
	 * @param $appVersion
	 * @return $this
	 */
	public function setAppVersion($appVersion)
	{
		$this->appVersion = $appVersion;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getAppId()
	{
		return $this->appId;
	}

	/**
	 * @param $appId
	 * @return $this
	 */
	public function setAppId($appId)
	{
		$this->appId = $appId;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getAppInstallerId()
	{
		return $this->appInstallerId;
	}

	/**
	 * @param $appInstallerId
	 * @return $this
	 */
	public function setAppInstallerId($appInstallerId)
	{
		$this->appInstallerId = $appInstallerId;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getScreenName()
	{
		return $this->screenName;
	}

	/**
	 * @param $screenName
	 * @return $this
	 */
	public function setScreenName($screenName)
	{
		$this->screenName = $screenName;

		return $this;
	}
}