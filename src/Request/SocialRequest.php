<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 19/10/2018
 * Time: 13:32
 */

namespace Peralada\Google\GoogleAnalytics\Request;


class SocialRequest extends AbstractBaseRequest
{
	const HIT_TYPE_SOCIAL = 'social';

	protected $socialAction;
	protected $socialNetwork;
	protected $socialTarget;

	public function __construct($gateway)
	{
		$this->setT(self::HIT_TYPE_SOCIAL);
		parent::__construct($gateway);
	}

	protected function createRequest()
	{
		$request = [
			'sa' => $this->getSocialAction(),
			'sn' => $this->getSocialNetwork(),
			'st' => $this->getSocialTarget()
		];

		return $request;
	}

	/**
	 * @return mixed
	 */
	public function getSocialAction()
	{
		return $this->socialAction;
	}

	/**
	 * @param $socialAction
	 * @return $this
	 */
	public function setSocialAction($socialAction)
	{
		$this->socialAction = $socialAction;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getSocialNetwork()
	{
		return $this->socialNetwork;
	}

	/**
	 * @param $socialNetwork
	 * @return $this
	 */
	public function setSocialNetwork($socialNetwork)
	{
		$this->socialNetwork = $socialNetwork;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getSocialTarget()
	{
		return $this->socialTarget;
	}

	/**
	 * @param $socialTarget
	 * @return $this
	 */
	public function setSocialTarget($socialTarget)
	{
		$this->socialTarget = $socialTarget;

		return $this;
	}
}