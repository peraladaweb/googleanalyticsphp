<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 22/10/2018
 * Time: 9:47
 */

namespace Peralada\Google\GoogleAnalytics\Request;


class TimingRequest extends  AbstractBaseRequest
{

	const HIT_TYPE_TIMING = 'timing';

	protected $category;
	protected $variable;
	protected $time;
	protected $label;

	protected $dnsLoadTime; //opcional
	protected $pageDownloadTime; //opcional
	protected $redirectTime; //opcional
	protected $tcpConnectTime; //opcional
	protected $serverResponseTime; //opcional

	public function __construct($gateway)
	{
		$this->setT(self::HIT_TYPE_TIMING);
		parent::__construct($gateway);
	}

	protected function createRequest()
	{
		$request = [
			'utc' => $this->getCategory(),
			'utv' => $this->getVariable(),
			'utt' => $this->getTime(),
			'utl' => $this->getLabel()
		];

		if (!empty($this->getDnsLoadTime())) {
			$request['dns'] = $this->getDnsLoadTime();
		}

		if (!empty($this->getPageDownloadTime())) {
			$request['pdt'] = $this->getPageDownloadTime();
		}

		if (!empty($this->getRedirectTime())) {
			$request['rrt'] = $this->getRedirectTime();
		}

		if (!empty($this->getTcpConnectTime())) {
			$request['tcp'] = $this->getTcpConnectTime();
		}

		if (!empty($this->getServerResponseTime())) {
			$request['srt'] = $this->getServerResponseTime();
		}

		return $request;
	}

	/**
	 * @return mixed
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * @param $category
	 * @return $this
	 */
	public function setCategory($category)
	{
		$this->category = $category;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getVariable()
	{
		return $this->variable;
	}

	/**
	 * @param $variable
	 * @return $this
	 */
	public function setVariable($variable)
	{
		$this->variable = $variable;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTime()
	{
		return $this->time;
	}

	/**
	 * @param $time
	 * @return $this
	 */
	public function setTime($time)
	{
		$this->time = $time;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * @param $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		$this->label = $label;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDnsLoadTime()
	{
		return $this->dnsLoadTime;
	}

	/**
	 * @param $dnsLoadTime
	 * @return $this
	 */
	public function setDnsLoadTime($dnsLoadTime)
	{
		$this->dnsLoadTime = $dnsLoadTime;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPageDownloadTime()
	{
		return $this->pageDownloadTime;
	}

	/**
	 * @param $pageDownloadTime
	 * @return $this
	 */
	public function setPageDownloadTime($pageDownloadTime)
	{
		$this->pageDownloadTime = $pageDownloadTime;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getRedirectTime()
	{
		return $this->redirectTime;
	}

	/**
	 * @param $redirectTime
	 * @return $this
	 */
	public function setRedirectTime($redirectTime)
	{
		$this->redirectTime = $redirectTime;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTcpConnectTime()
	{
		return $this->tcpConnectTime;
	}

	/**
	 * @param $tcpConnectTime
	 * @return $this
	 */
	public function setTcpConnectTime($tcpConnectTime)
	{
		$this->tcpConnectTime = $tcpConnectTime;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getServerResponseTime()
	{
		return $this->serverResponseTime;
	}

	/**
	 * @param $serverResponseTime
	 * @return $this
	 */
	public function setServerResponseTime($serverResponseTime)
	{
		$this->serverResponseTime = $serverResponseTime;

		return $this;
	}


}