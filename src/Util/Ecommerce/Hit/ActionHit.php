<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 03/10/2018
 * Time: 11:58
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit;


use Peralada\Google\GoogleAnalytics\Util\Ecommerce\ProductList;

class ActionHit implements EcommerceHitInterface
{
	const PRODUCT_ACTION_DETAIL = 'detail';
	const PRODUCT_ACTION_CLICK = 'click';
	const PRODUCT_ACTION_ADD = 'add';
	const PRODUCT_ACTION_REMOVE = 'remove';
	const PRODUCT_ACTION_CHECKOUT = 'checkout';
	const PRODUCT_ACTION_CHECKOUT_OPTION = 'checkout_option';
	const PRODUCT_ACTION_PURCHASE = 'purchase';
	const PRODUCT_ACTION_REFUND = 'refund';

	protected $action;
	protected $list;

	public function getRequest()
	{
		$request = [];

		$request['pa'] = $this->action;
		$listRequest = $this->createListResquest();

		$request = array_merge($request, $listRequest);

		return $request;
	}

	protected function createListResquest()
	{
		$request = [];

		if (!empty($this->list->getProducts())) {
			$request['pal'] = $this->list->getName();

			$productsRequest = $this->createProductsRequest($this->list->getProducts());

			$request = array_merge($request, $productsRequest);
		}

		return $request;
	}

	protected function createProductsRequest($products)
	{
		$request = [];

		$productCount = 1;
		foreach ($products as $product) {
			if (!empty($product->getId())) {
				$request['pr' . $productCount . 'id'] = $product->getId();
			}
			$request['pr' . $productCount . 'nm'] = $product->getName();
			if (!empty($product->getCategory())) {
				$request['pr' . $productCount . 'ca'] = $product->getCategory();
			}
			if (!empty($product->getBrand())) {
				$request['pr' . $productCount . 'br'] = $product->getBrand();
			}
			if (!empty($product->getVariant())) {
				$request['pr' . $productCount . 'va'] = $product->getVariant();
			}
			if (!empty($product->getPosition())) {
				$request['pr' . $productCount . 'ps'] = $product->getPosition();
			}
//					$request['pr' . $productCount . 'cd1'] = $product->getDimension();

			$productCount++;
		}

		return $request;
	}

	/**
	 * @return mixed
	 */
	public function getAction()
	{
		return $this->action;
	}

	/**
	 * @param $action
	 * @return $this
	 */
	public function setAction($action)
	{
		$this->action = $action;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getList()
	{
		return $this->list;
	}

	/**
	 * @param $list
	 * @return $this
	 */
	public function setList(ProductList $list)
	{
		$this->list = $list;

		return $this;
	}
}