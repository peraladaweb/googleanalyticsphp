<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 04/10/2018
 * Time: 13:25
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit;


class CheckoutStepHit implements EcommerceHitInterface
{
	protected $checkoutStep;
	protected $checkoutStepOption;
	protected $actionHit;

	public function getRequest()
	{
		$request = $this->createCheckoutRequest();
		$actionHitReuquest = $this->actionHit->getRequest();

		$request = array_merge($request, $actionHitReuquest);

		return $request;
	}

	public function createCheckoutRequest()
	{
		$request = [];

		$request['cos'] = $this->checkoutStep;
		$request['col'] = $this->checkoutStepOption;

		return $request;
	}

	/**
	 * @return mixed
	 */
	public function getActionHit()
	{
		return $this->actionHit;
	}

	/**
	 * @param ActionHit $action
	 * @return $this
	 */
	public function setActionHit(ActionHit $action)
	{
		$this->actionHit = $action;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCheckoutStep()
	{
		return $this->checkoutStep;
	}

	/**
	 * @param $checkoutStep
	 * @return $this
	 */
	public function setCheckoutStep($checkoutStep)
	{
		$this->checkoutStep = $checkoutStep;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCheckoutStepOption()
	{
		return $this->checkoutStepOption;
	}

	/**
	 * @param $checkoutStepOption
	 * @return $this
	 */
	public function setCheckoutStepOption($checkoutStepOption)
	{
		$this->checkoutStepOption = $checkoutStepOption;

		return $this;
	}


}