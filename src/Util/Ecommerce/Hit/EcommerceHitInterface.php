<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 03/10/2018
 * Time: 11:55
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit;


interface EcommerceHitInterface
{
	public function getRequest();
}