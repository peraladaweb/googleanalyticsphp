<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 03/10/2018
 * Time: 11:58
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit;

use Peralada\Google\GoogleAnalytics\Util\Ecommerce\ProductList;

class ImpressionHit implements EcommerceHitInterface
{
	protected $lists;

	public function getRequest()
	{
		$listRequest = $this->createListsRequest();

		return $listRequest;
	}

	protected function createListsRequest()
	{
		$request = [];

		$listCount = 1;
		foreach ($this->lists as $list) {
			if (!empty($list->getProducts())) {
				$request['il' . $listCount . 'nm'] = $list->getName();

				$productsRequest = $this->createProductsRequest($list->getProducts(), $listCount);

				$request = array_merge($request, $productsRequest);

				$listCount ++;
			}
		}

		return $request;
	}

	protected function createProductsRequest($products, $listCount)
	{
		$request = [];

		$productCount = 1;
		foreach ($products as $product) {
			if (!empty($product->getId())) {
				$request['il' . $listCount . 'pi' . $productCount . 'id'] = $product->getId();
			}
			$request['il' . $listCount . 'pi' . $productCount . 'nm'] = $product->getName();
			if (!empty($product->getCategory())) {
				$request['il' . $listCount . 'pi' . $productCount . 'ca'] = $product->getCategory();
			}
			if (!empty($product->getBrand())) {
				$request['il' . $listCount . 'pi' . $productCount . 'br'] = $product->getBrand();
			}
			if (!empty($product->getVariant())) {
				$request['il' . $listCount . 'pi' . $productCount . 'va'] = $product->getVariant();
			}
			if (!empty($product->getPosition())) {
				$request['il' . $listCount . 'pi' . $productCount . 'ps'] = $product->getPosition();
			}
//					$request['il' . $listCount . 'pi' . $productCount . 'cd1'] = $product->getDimension();

			$productCount ++;
		}

		return $request;
	}

	public function addList(ProductList $list)
	{
		$this->lists[] = $list;

		return $this->lists;
	}

	/**
	 * @return array
	 */
	public function getLists()
	{
		return $this->lists;
	}

	/**
	 * @param array $lists
	 */
	public function setLists($lists)
	{
		$this->lists = $lists;
	}
}