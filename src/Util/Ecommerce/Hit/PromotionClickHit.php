<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 16/10/2018
 * Time: 12:22
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit;


class PromotionClickHit implements EcommerceHitInterface
{
	use PromotionsTrait;

	const PROMOTION_ACTION = 'click';

	protected $promotions;

	public function __construct()
	{
		$this->promotions = [];
	}

	public function getRequest()
	{
		$promotionClickRequest = [];
		$promotionClickRequest['promoa'] = self::PROMOTION_ACTION;
		$promotionsRequest = $this->createPromotionsRequest();

		$promotionClickRequest = array_merge($promotionClickRequest, $promotionsRequest);

		return $promotionClickRequest;
	}

	/**
	 * @return array
	 */
	public function getPromotions()
	{
		return $this->promotions;
	}

	/**
	 * @param array $promotions
	 */
	public function setPromotions($promotions)
	{
		$this->promotions = $promotions;
	}
}