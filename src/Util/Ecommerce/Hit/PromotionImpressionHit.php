<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 16/10/2018
 * Time: 11:12
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit;


class PromotionImpressionHit implements EcommerceHitInterface
{
	use PromotionsTrait;

	protected $promotions;

	public function __construct()
	{
		$this->promotions = [];
	}

	public function getRequest()
	{
		$promotionsRequest = $this->createPromotionsRequest();

		return $promotionsRequest;
	}

	/**
	 * @return array
	 */
	public function getPromotions()
	{
		return $this->promotions;
	}

	/**
	 * @param array $promotions
	 */
	public function setPromotions($promotions)
	{
		$this->promotions = $promotions;
	}


}