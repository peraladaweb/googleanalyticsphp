<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 19/10/2018
 * Time: 10:11
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit;


trait PromotionsTrait
{
	protected function createPromotionsRequest()
	{
		$index = 1;
		$request = [];

		foreach ($this->promotions as $promotion) {
			$request['promo' . $index . 'id'] = $promotion->getId();
			$request['promo' . $index . 'nm'] = $promotion->getName();
			$request['promo' . $index . 'cr'] = $promotion->getCreative();
			$request['promo' . $index . 'ps'] = $promotion->getPosition();

			$index ++;
		}

		return $request;
	}
}