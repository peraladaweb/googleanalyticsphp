<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 03/10/2018
 * Time: 16:43
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit;


use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Transaction;

class TransactionHit implements EcommerceHitInterface
{
	/**
	 * @var ActionHit
	 */
	protected $actionHit;

	/**
	 * @var Transaction
	 */
	protected $transaction;

	/**
	 * @return array
	 */
	public function getRequest()
	{
		$request = $this->createTransactionRequest();
		$actionHitReuquest = $this->actionHit->getRequest();

		$request = array_merge($request, $actionHitReuquest);

		return $request;
	}

	/**
	 * @return array
	 */
	protected function createTransactionRequest()
	{
		$request = [];

		$request['ti'] = $this->transaction->getTransactionId();
		$request['ta'] = $this->transaction->getAffiliation();
		$request['tr'] = $this->transaction->getRevenue();
		$request['tt'] = $this->transaction->getTax();
		$request['ts'] = $this->transaction->getShipping();
		$request['tcc'] = $this->transaction->getTransactionCoupon();

		return $request;
	}

	/**
	 * @return ActionHit
	 */
	public function getActionHit()
	{
		return $this->actionHit;
	}

	/**
	 * @param ActionHit $actionHit
	 * @return $this
	 */
	public function setActionHit(ActionHit $actionHit)
	{
		$this->actionHit = $actionHit;

		return $this;
	}

	/**
	 * @return Transaction
	 */
	public function getTransaction()
	{
		return $this->transaction;
	}

	/**
	 * @param Transaction $transaction
	 * @return $this
	 */
	public function setTransaction(Transaction $transaction)
	{
		$this->transaction = $transaction;

		return $this;
	}
}