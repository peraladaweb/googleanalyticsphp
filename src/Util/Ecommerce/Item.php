<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 19/10/2018
 * Time: 12:45
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce;


class Item
{
	protected $name;
	protected $price;
	protected $quantity;
	protected $code;
	protected $variation;

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param $name
	 * @return $this
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * @param $price
	 * @return $this
	 */
	public function setPrice($price)
	{
		$this->price = $price;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * @param $quantity
	 * @return $this
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * @param $code
	 * @return $this
	 */
	public function setCode($code)
	{
		$this->code = $code;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getVariation()
	{
		return $this->variation;
	}

	/**
	 * @param $variation
	 * @return $this
	 */
	public function setVariation($variation)
	{
		$this->variation = $variation;

		return $this;
	}
}