<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 20/09/2018
 * Time: 10:22
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce;


class Product
{
	/**
	 * @var string
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $category;

	/**
	 * @var string
	 */
	protected $brand;

	/**
	 * @var string
	 */
	protected $variant;

	/**
	 * @var string
	 */
	protected $position;

	/**
	 * @var string
	 */
	protected $dimension;

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * @param string $category
	 */
	public function setCategory($category)
	{
		$this->category = $category;
	}

	/**
	 * @return string
	 */
	public function getBrand()
	{
		return $this->brand;
	}

	/**
	 * @param string $brand
	 */
	public function setBrand($brand)
	{
		$this->brand = $brand;
	}

	/**
	 * @return string
	 */
	public function getVariant()
	{
		return $this->variant;
	}

	/**
	 * @param string $variant
	 */
	public function setVariant($variant)
	{
		$this->variant = $variant;
	}

	/**
	 * @return string
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * @param string $position
	 */
	public function setPosition($position)
	{
		$this->position = $position;
	}

	/**
	 * @return string
	 */
	public function getDimension()
	{
		return $this->dimension;
	}

	/**
	 * @param string $dimension
	 */
	public function setDimension($dimension)
	{
		$this->dimension = $dimension;
	}
}