<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 20/09/2018
 * Time: 10:59
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce;


class ProductList
{
	protected $name;
	protected $products;

	public function __construct()
	{
		$this->products = [];
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	public function addProduct(Product $product)
	{
		$this->products[] = $product;

		return $this->products;
	}

	/**
	 * @return array
	 */
	public function getProducts()
	{
		return $this->products;
	}

	/**
	 * @param array $products
	 */
	public function setProducts($products)
	{
		$this->products = $products;
	}

}