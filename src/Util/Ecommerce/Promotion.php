<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 16/10/2018
 * Time: 11:09
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce;


class Promotion
{
	protected $id;
	protected $name;
	protected $creative;
	protected $position;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 *
	 * @return $this
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCreative()
	{
		return $this->creative;
	}

	/**
	 * @param mixed $creative
	 *
	 * @return $this
	 */
	public function setCreative($creative)
	{
		$this->creative = $creative;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * @param mixed $position
	 *
	 * @return $this
	 */
	public function setPosition($position)
	{
		$this->position = $position;

		return $this;
	}

}