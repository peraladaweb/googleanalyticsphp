<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 04/10/2018
 * Time: 9:24
 */

namespace Peralada\Google\GoogleAnalytics\Util\Ecommerce;


class Transaction
{
	const CURRENCY_CODE_EUR = 'EUR';

	protected $transactionId;
	protected $affiliation;
	protected $revenue;
	protected $tax;
	protected $shipping;
	protected $transactionCoupon;
	protected $currency;

	/**
	 * @return mixed
	 */
	public function getTransactionId()
	{
		return $this->transactionId;
	}

	/**
	 * @param $transactionId
	 * @return $this
	 */
	public function setTransactionId($transactionId)
	{
		$this->transactionId = $transactionId;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getAffiliation()
	{
		return $this->affiliation;
	}

	/**
	 * @param $affiliation
	 * @return $this
	 */
	public function setAffiliation($affiliation)
	{
		$this->affiliation = $affiliation;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getRevenue()
	{
		return $this->revenue;
	}

	/**
	 * @param $revenue
	 * @return $this
	 */
	public function setRevenue($revenue)
	{
		$this->revenue = $revenue;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTax()
	{
		return $this->tax;
	}

	/**
	 * @param $tax
	 * @return $this
	 */
	public function setTax($tax)
	{
		$this->tax = $tax;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getShipping()
	{
		return $this->shipping;
	}

	/**
	 * @param $shipping
	 * @return $this
	 */
	public function setShipping($shipping)
	{
		$this->shipping = $shipping;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTransactionCoupon()
	{
		return $this->transactionCoupon;
	}

	/**
	 * @param $transactionCoupon
	 * @return $this
	 */
	public function setTransactionCoupon($transactionCoupon)
	{
		$this->transactionCoupon = $transactionCoupon;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCurrency()
	{
		return $this->currency;
	}

	/**
	 * @param $currency
	 * @return $this
	 */
	public function setCurrency($currency)
	{
		$this->currency = $currency;

		return $this;
	}
}