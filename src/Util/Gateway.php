<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 10/08/2018
 * Time: 12:32
 */

namespace Peralada\Google\GoogleAnalytics\Util;


class Gateway
{
	protected $v;
	protected $tid;
	protected $cid;
	protected $url;

	public function __construct($cid, $tid, $url)
	{
		$this->v = 1;
		$this->cid = $cid;
		$this->tid = $tid;
		$this->url = $url;
	}

	/**
	 * @return mixed
	 */
	public function getV()
	{
		return $this->v;
	}

	/**
	 * @param $v
	 * @return $this
	 */
	public function setV($v)
	{
		$this->v = $v;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTid()
	{
		return $this->tid;
	}

	/**
	 * @param $tid
	 * @return $this
	 */
	public function setTid($tid)
	{
		$this->tid = $tid;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCid()
	{
		return $this->cid;
	}

	/**
	 * @param $cid
	 * @return $this
	 */
	public function setCid($cid)
	{
		$this->cid = $cid;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param $url
	 * @return $this
	 */
	public function setUrl($url)
	{
		$this->url = $url;

		return $this;
	}


}