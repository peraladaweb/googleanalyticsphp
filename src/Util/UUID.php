<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 09/08/2018
 * Time: 10:01
 */

class UUID {

	public static function getIdV4() {
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

			mt_rand(0, 0xffff),
			mt_rand(0, 0xffff),

			mt_rand(0, 0xffff),

			mt_rand(0, 0x0fff) | 0x4000,

			mt_rand(0, 0x3fff) | 0x8000,

			mt_rand(0, 0xffff),
			mt_rand(0, 0xffff),
			mt_rand(0, 0xffff)
		);
	}

	public static function isValid($uuid) {
		return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
				'[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
	}
}