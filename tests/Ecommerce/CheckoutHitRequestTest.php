<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 04/10/2018
 * Time: 16:23
 */

namespace Peralada\Google\GoogleAnalytics\Ecommerce;


use Peralada\Google\GoogleAnalytics\Parameters;
use Peralada\Google\GoogleAnalytics\Request\Ecommerce\EcommerceImprovedRequest;
use Peralada\Google\GoogleAnalytics\Request\PageViewRequest;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit\ActionHit;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit\CheckoutStepHit;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Product;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\ProductList;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

class CheckoutHitRequestTest extends TestCase
{
	/**
	 * @dataProvider requestProvider
	 */
	public function testCheckoutRequest($list)
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);

		$actionHit = new ActionHit();
		$actionHit->setAction(ActionHit::PRODUCT_ACTION_CHECKOUT)
			->setList($list);

		$chekoutHit = new CheckoutStepHit();
		$chekoutHit->setActionHit($actionHit)
			->setCheckoutStep(1)
			->setCheckoutStepOption('Formulario Tarifa');

		$pageViewRequest = new PageViewRequest($gateway);
		$pageViewRequest->setDocumentPage('/test-home');
		$pageViewRequest->setDocumentTitle('Test-Home');

		$ecommerceRequest = new EcommerceImprovedRequest($gateway, $pageViewRequest);
		$ecommerceRequest->addHit($chekoutHit);

		$this->assertEquals(
			true,
			$ecommerceRequest->request()->hitParsingResult[0]->valid
		);
	}

	/**
	 * @dataProvider requestProvider
	 */
	public function testCheckoutOptionRequest($list)
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);

		$actionHit = new ActionHit();
		$actionHit->setAction(ActionHit::PRODUCT_ACTION_CHECKOUT_OPTION)
			->setList($list);

		$chekoutHit = new CheckoutStepHit();
		$chekoutHit->setActionHit($actionHit)
			->setCheckoutStep(1)
			->setCheckoutStepOption('FedEx');

		$pageViewRequest = new PageViewRequest($gateway);
		$pageViewRequest->setDocumentPage('/test-home');
		$pageViewRequest->setDocumentTitle('Test-Home');

		$ecommerceRequest = new EcommerceImprovedRequest($gateway, $pageViewRequest);
		$ecommerceRequest->addHit($chekoutHit);

		$this->assertEquals(
			true,
			$ecommerceRequest->request()->hitParsingResult[0]->valid
		);
	}

	public function requestProvider()
	{
		$parametersOneProduct = [];
		$parametersMoreOneProduct = [];
		$parametersOneProduct[] = $this->createList(1);
		$parametersMoreOneProduct[] = $this->createList(3);

		return [
			'one Product Action' => $parametersOneProduct,
			'more than one Product Action' => $parametersMoreOneProduct
		];
	}

	protected function createList($number)
	{
		$list = new ProductList();
		$list->setName('Lista');
		$list->setProducts($this->createProducts($number));

		return $list;
	}

	protected function createProducts($number)
	{
		$products = [];

		for ($i = 1; $i < $number + 1; $i++) {
			$product = new Product();
			$product->setId((new \DateTime())->getTimestamp());
			$product->setName('Nombre Producto' . $i);
			$product->setCategory('Categoria Producto');
			$product->setBrand('Marca Producto');
			$product->setVariant('Variante Producto');

			$products[] = $product;
		}

		return $products;
	}

}