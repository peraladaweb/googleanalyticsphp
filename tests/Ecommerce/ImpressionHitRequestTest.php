<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 20/09/2018
 * Time: 12:26
 */

namespace Peralada\Google\GoogleAnalytics\Ecommerce;


use Peralada\Google\GoogleAnalytics\Parameters;
use Peralada\Google\GoogleAnalytics\Request\Ecommerce\EcommerceImprovedRequest;
use Peralada\Google\GoogleAnalytics\Request\PageViewRequest;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit\ImpressionHit;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Product;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\ProductList;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

class ImpressionHitRequestTest extends TestCase
{
	/**
	 * @dataProvider requestProvider
	 */
	public function testRequest($lists)
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);


		$impression = new ImpressionHit();
		$impression->setLists($lists);

		$pageViewRequest = new PageViewRequest($gateway);
		$pageViewRequest->setDocumentPage('/test-home');
		$pageViewRequest->setDocumentTitle('Test-Home');

		$ecommerceRequest = new EcommerceImprovedRequest($gateway, $pageViewRequest);
		$ecommerceRequest->addHit($impression);



		$this->assertEquals(
			true,
			$ecommerceRequest->request()->hitParsingResult[0]->valid
		);
	}

	public function requestProvider()
	{
		$firstList = [];
		$secondList = [];

		$firstList[] = $this->createLists(1);
		$secondList[] = $this->createLists(2);

		return [
			'one list' => $firstList,
			'two lists' => $secondList
		];
	}

	/**
	 * @param $number
	 * @return array
	 */
	protected function createLists($number)
	{
		$lists = [];

		for ($i = 1; $i < $number + 1; $i ++) {
			$list = new ProductList();
			$list->setName('Primera Lista');
			$list->setProducts($this->createProducts($i));

			$lists[] = $list;
		}

		return $lists;
	}

	protected function createProducts($number)
	{
		$products = [];

		for ($i = 1; $i < $number + 1; $i ++) {
			$product = new Product();
			$product->setId((new \DateTime())->getTimestamp());
			$product->setName('Nombre Producto' . $i);
			$product->setCategory('Categoria Producto');
			$product->setBrand('Marca Producto');
			$product->setVariant('Variante Producto');

			$products[] = $product;
		}

		return $products;
	}
}