<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 19/10/2018
 * Time: 12:54
 */

namespace Peralada\Google\GoogleAnalytics\Ecommerce;


use Peralada\Google\GoogleAnalytics\Parameters;
use Peralada\Google\GoogleAnalytics\Request\Ecommerce\ItemRequest;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Item;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Transaction;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

class ItemRequestTest extends TestCase
{
	public function testRequest()
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);

		$transaction = new Transaction();

		$transaction->setTransactionId('123456')
			->setAffiliation('westernWear')
			->setRevenue(50.00)
			->setShipping(32.00)
			->setTax(12.00)
			->setCurrency(Transaction::CURRENCY_CODE_EUR)
		;

		$item = new Item();
		$item->setName('sofa')
			->setPrice(300)
			->setQuantity(2)
			->setCode('u3eqds43 ')
			->setVariation('furniture');


		$ItemRequest = new ItemRequest($gateway);
		$ItemRequest->setTransaction($transaction)
			->setItem($item);

		$this->assertEquals(
			true,
			$ItemRequest->request()->hitParsingResult[0]->valid
		);
	}

}