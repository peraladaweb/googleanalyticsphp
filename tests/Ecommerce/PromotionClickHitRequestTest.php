<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 19/10/2018
 * Time: 10:21
 */

namespace Peralada\Google\GoogleAnalytics\Ecommerce;


use Peralada\Google\GoogleAnalytics\Parameters;
use Peralada\Google\GoogleAnalytics\Request\Ecommerce\EcommerceImprovedRequest;
use Peralada\Google\GoogleAnalytics\Request\EventRequest;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit\PromotionClickHit;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Promotion;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

class PromotionClickHitRequestTest
{
	/**
	* @dataProvider requestProvider
	*/
	public function testRequest($promotions)
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);

		$promotionClickHit = new PromotionClickHit();
		$promotionClickHit->setPromotions($promotions);

		$eventRequest = new EventRequest($gateway);
		$eventRequest->setEventCategory('test-category')
			->setEventAction('test-Action')
			->setEventLabel('test-lavel')
			->setEventValue('0');

		$ecommerceRequest = new EcommerceImprovedRequest($gateway, $eventRequest);
		$ecommerceRequest->addHit($promotionClickHit);

		$this->assertEquals(
			true,
			$ecommerceRequest->request()->hitParsingResult[0]->valid
		);
	}

	public function requestProvider()
	{
		$parametersOnePromotion = [];
		$parametersMoreOnePromotion = [];
		$parametersOnePromotion[] = $this->createPromotions(1);
		$parametersMoreOnePromotion[] = $this->createPromotions(3);

		return [
			'one Promotion' => $parametersOnePromotion,
			'more than one Promotion' => $parametersMoreOnePromotion
		];
	}

	protected function createPromotions($number)
	{
		$promotions = [];

		for ($i = 1; $i < $number + 1; $i++) {
			$promotion = new Promotion();
			$promotion->setId('PROMO_1234');
			$promotion->setName('PROMO NAME');
			$promotion->setCreative('PROMO CREATIVE');
			$promotion->setPosition('PROMO POSITION 1');

			$promotions[] = $promotion;
		}

		return $promotions;
	}
}