<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 16/10/2018
 * Time: 11:33
 */

namespace Peralada\Google\GoogleAnalytics\Ecommerce;


use Peralada\Google\GoogleAnalytics\Parameters;
use Peralada\Google\GoogleAnalytics\Request\Ecommerce\EcommerceImprovedRequest;
use Peralada\Google\GoogleAnalytics\Request\PageViewRequest;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit\PromotionImpressionHit;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Promotion;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

class PromotionImpressionHitRequestTest extends TestCase
{
	/**
	 * @dataProvider requestProvider
	 */
	public function testRequest($promotions)
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);

		$promotionImpressionHit = new PromotionImpressionHit();
		$promotionImpressionHit->setPromotions($promotions);

		$pageViewRequest = new PageViewRequest($gateway);
		$pageViewRequest->setDocumentPage('/test-home');
		$pageViewRequest->setDocumentTitle('Test-Home');

		$ecommerceRequest = new EcommerceImprovedRequest($gateway, $pageViewRequest);
		$ecommerceRequest->addHit($promotionImpressionHit);

		$this->assertEquals(
			true,
			$ecommerceRequest->request()->hitParsingResult[0]->valid
		);
	}

	public function requestProvider()
	{
		$parametersOnePromotion = [];
		$parametersMoreOnePromotion = [];
		$parametersOnePromotion[] = $this->createPromotions(1);
		$parametersMoreOnePromotion[] = $this->createPromotions(3);

		return [
			'one Promotion' => $parametersOnePromotion,
			'more than one Promotion' => $parametersMoreOnePromotion
		];
	}

	protected function createPromotions($number)
	{
		$promotions = [];

		for ($i = 1; $i < $number + 1; $i++) {
			$promotion = new Promotion();
			$promotion->setId('PROMO_1234');
			$promotion->setName('PROMO NAME');
			$promotion->setCreative('PROMO CREATIVE');
			$promotion->setPosition('PROMO POSITION 1');

			$promotions[] = $promotion;
		}

		return $promotions;
	}
}