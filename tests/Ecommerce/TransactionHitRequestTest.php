<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 04/10/2018
 * Time: 10:39
 */

namespace Peralada\Google\GoogleAnalytics\Ecommerce;


use Peralada\Google\GoogleAnalytics\Parameters;
use Peralada\Google\GoogleAnalytics\Request\Ecommerce\EcommerceImprovedRequest;
use Peralada\Google\GoogleAnalytics\Request\PageViewRequest;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit\ActionHit;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Hit\TransactionHit;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Product;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\ProductList;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Transaction;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

class TransactionHitRequestTest extends TestCase
{
	/**
	 * @dataProvider requestProvider
	 */
	public function testRequest($list, $transaction)
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);

		$actionHit = new ActionHit();
		$actionHit->setAction(ActionHit::PRODUCT_ACTION_CLICK)
			->setList($list);

		$transactionHit = new TransactionHit();
		$transactionHit->setTransaction($transaction);
		$transactionHit->setActionHit($actionHit);

		$pageViewRequest = new PageViewRequest($gateway);
		$pageViewRequest->setDocumentPage('/test-home');
		$pageViewRequest->setDocumentTitle('Test-Home');

		$ecommerceRequest = new EcommerceImprovedRequest($gateway, $pageViewRequest);
		$ecommerceRequest->addHit($transactionHit);

		$this->assertEquals(
			true,
			$ecommerceRequest->request()->hitParsingResult[0]->valid
		);
	}

	public function requestProvider()
	{
		$transaction = $this->createTransaction();
		$parametersOneProduct = [];
		$parametersMoreOneProduct = [];
		$parametersOneProduct[] = $this->createList(1);
		$parametersOneProduct[] = $transaction;
		$parametersMoreOneProduct[] = $this->createList(3);
		$parametersMoreOneProduct[] = $transaction;

		return [
			'one Product Action' => $parametersOneProduct,
			'more than one Product Action' => $parametersMoreOneProduct
		];
	}

	protected function createTransaction()
	{
		$transaction = new Transaction();
		$transaction->setTransactionId('transaction id test')
			->setAffiliation('Affiliation test')
			->setRevenue(37.39)
			->setTax(2.85)
			->setShipping(5.34)
			->setTransactionCoupon('transaction coupon test');

		return $transaction;
	}

	protected function createList($number)
	{
		$list = new ProductList();
		$list->setName('Lista');
		$list->setProducts($this->createProducts($number));

		return $list;
	}

	protected function createProducts($number)
	{
		$products = [];

		for ($i = 1; $i < $number + 1; $i++) {
			$product = new Product();
			$product->setId((new \DateTime())->getTimestamp());
			$product->setName('Nombre Producto' . $i);
			$product->setCategory('Categoria Producto');
			$product->setBrand('Marca Producto');
			$product->setVariant('Variante Producto');

			$products[] = $product;
		}

		return $products;
	}
}