<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 19/10/2018
 * Time: 12:36
 */

namespace Peralada\Google\GoogleAnalytics\Ecommerce;


use Peralada\Google\GoogleAnalytics\Parameters;
use Peralada\Google\GoogleAnalytics\Request\Ecommerce\TransactionRequest;
use Peralada\Google\GoogleAnalytics\Util\Ecommerce\Transaction;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

class TransactionRequestTest extends TestCase
{
	public function testRequest()
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);

		$transaction = new Transaction();

		$transaction->setTransactionId('123456')
			->setAffiliation('westernWear')
			->setRevenue(50.00)
			->setShipping(32.00)
			->setTax(12.00)
			->setCurrency(Transaction::CURRENCY_CODE_EUR)
		;


		$transactionRequest = new TransactionRequest($gateway);
		$transactionRequest->setTransaction($transaction);

		$this->assertEquals(
			true,
			$transactionRequest->request()->hitParsingResult[0]->valid
		);
	}
}