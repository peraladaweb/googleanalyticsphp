<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 17/09/2018
 * Time: 9:45
 */

namespace Peralada\Google\GoogleAnalytics;


use Peralada\Google\GoogleAnalytics\Request\EventRequest;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

class EventTest extends TestCase
{
	public function testRequest()
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);
		$event = new EventRequest($gateway);
		$event->setEventCategory('test-category')
			->setEventAction('test-Action')
			->setEventLabel('test-lavel')
			->setEventValue('0');

		$this->assertEquals(
			true,
			$event->request()->hitParsingResult[0]->valid
		);
	}

	public function testRequestObligatoryParameters()
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);
		$event = new EventRequest($gateway);
		$event->setEventCategory('test-category')
			->setEventAction('test-Action');

		$this->assertEquals(
			true,
			$event->request()->hitParsingResult[0]->valid
		);
	}
}