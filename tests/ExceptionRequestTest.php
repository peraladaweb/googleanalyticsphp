<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 19/10/2018
 * Time: 13:49
 */

namespace Peralada\Google\GoogleAnalytics;


use Peralada\Google\GoogleAnalytics\Request\ExceptionRequest;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

class ExceptionRequestTest  extends TestCase
{
	public function testRequest()
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);

		$exceptionRequest = new ExceptionRequest($gateway);
		$exceptionRequest->setExceptionDescription('IOException')
			->setExceptionIsFatal(1);

		$this->assertEquals(
			true,
			$exceptionRequest->request()->hitParsingResult[0]->valid
		);
	}
}