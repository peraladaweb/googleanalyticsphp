<?php
namespace Peralada\Google\GoogleAnalytics\Tests;


use Peralada\Google\GoogleAnalytics\Parameters;
use Peralada\Google\GoogleAnalytics\Request\PageViewRequest;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 13/09/2018
 * Time: 9:27
 */

class PageViewTest extends TestCase
{
	public function testRequest()
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);
		$pageView = new PageViewRequest($gateway);
		$pageView->setDocumentPage('/test-home');
		$pageView->setDocumentTitle('Test-Home');

		$this->assertEquals(
			true,
			$pageView->request()->hitParsingResult[0]->valid
		);
	}
}