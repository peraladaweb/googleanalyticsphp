<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 22/10/2018
 * Time: 10:48
 */

namespace Peralada\Google\GoogleAnalytics;


use Peralada\Google\GoogleAnalytics\Request\ScreenViewRequest;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

class ScreenViewRequestTest extends TestCase
{
	public function testRequest()
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);

		$screenViewRequest = new ScreenViewRequest($gateway);
		$screenViewRequest->setAppName('funTImes')
			->setAppVersion('1.5.0')
			->setAppId('com.peralada.analytics')
			->setAppInstallerId('com.peralada.analytics.app.installer')
			->setScreenName('Home');

		$this->assertEquals(
			true,
			$screenViewRequest->request()->hitParsingResult[0]->valid
		);
	}
}