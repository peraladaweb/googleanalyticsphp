<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 19/10/2018
 * Time: 13:42
 */

namespace Peralada\Google\GoogleAnalytics;


use Peralada\Google\GoogleAnalytics\Request\SocialRequest;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

class SocialRequestTest extends TestCase
{
	public function testRequest()
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);

		$socialRequest = new SocialRequest($gateway);
		$socialRequest->setSocialAction('like')
			->setSocialNetwork('facebook')
			->setSocialTarget('/home');

		$this->assertEquals(
			true,
			$socialRequest->request()->hitParsingResult[0]->valid
		);
	}
}