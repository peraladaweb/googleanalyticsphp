<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 22/10/2018
 * Time: 9:59
 */

namespace Peralada\Google\GoogleAnalytics;


use Peralada\Google\GoogleAnalytics\Request\TimingRequest;
use Peralada\Google\GoogleAnalytics\Util\Gateway;
use PHPUnit\Framework\TestCase;

class TimingRequestTest extends TestCase
{
	public function testRequiredFieldsRequest()
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);

		$timingRequest = new TimingRequest($gateway);
		$timingRequest->setCategory('jsonLoader')
			->setVariable('load')
			->setTime(5000)
			->setLabel('jQuery');

		$this->assertEquals(
			true,
			$timingRequest->request()->hitParsingResult[0]->valid
		);
	}

	public function testOptionalFieldsRequest()
	{
		$gateway = new Gateway(Parameters::CID, Parameters::TID, Parameters::URL);

		$timingRequest = new TimingRequest($gateway);
		$timingRequest->setCategory('jsonLoader')
			->setVariable('load')
			->setTime(5000)
			->setLabel('jQuery')
			->setDnsLoadTime(100)
			->setPageDownloadTime(20)
			->setRedirectTime(32)
			->setTcpConnectTime(56)
			->setServerResponseTime(12)
		;

		$this->assertEquals(
			true,
			$timingRequest->request()->hitParsingResult[0]->valid
		);
	}
}